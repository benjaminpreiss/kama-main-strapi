#!/bin/bash

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

rsync -av --stats --progress ${SERVER_PROD_SSH_USER}@${SERVER_PROD_IP}:${SERVER_PROD_PROJECT_PATH}/storage/public/uploads/ ${dir}/public/uploads