module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: env('STRAPI_DEV_DB_HOST', '127.0.0.1'),
      port: env.int('STRAPI_DEV_DB_PORT', 1234),
      database: env('STRAPI_DEV_DB_NAME', 'temp'),
      user: env('STRAPI_DEV_DB_USERNAME', 'temp'),
      password: env('STRAPI_DEV_DB_PASSWORD', 'temp'),
      ssl: env.bool('STRAPI_DEV_DB_SSL', true),
    },
  },
});
