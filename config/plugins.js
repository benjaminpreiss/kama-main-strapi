module.exports = ({ env }) => {
    return {
     ckeditor: {
      enabled: true,
      config:{
         editor:{ // editor default config
           // https://ckeditor.com/docs/ckeditor5/latest/features/markdown.html
           // if you need markdown support and output set it to removePlugins: [''],
           // default is removePlugins: ['Markdown'],
           // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html
           // lists of items: https://market.strapi.io/plugins/@_sh-strapi-plugin-ckeditor
           toolbar: {
             items: [
               'paragraph',
               'heading3',
               'heading','bold',
               'italic',
               'underline',
               'fontColor', 'code',
               'link',
               'insertTable',
               "fullScreen",
               'sourceEditing',
               'undo',
               'redo'
             ]
           },
           fontColor: {
            colors: [
              {
                color: '#A7A7A7',
                label: 'unterschrift-gray'
              },
            ]
           },
           table: {
            contentToolbar: [
              'tableColumn',
              'tableRow',
              'mergeTableCells',
              'tableCellProperties',
              'tableProperties'
            ]
          },
           heading: {
            options: [
              { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
              { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
              { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
              { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
              { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
              { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
              { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' },
            ]
          },

         }
       }
     }, 
     email: {
      config: {
        provider: 'nodemailer',
        providerOptions: {
          host: env('MAILJET_SMTP_HOST'),
          port: env('MAILJET_SMTP_PORT'),
          auth: {
            user: env('MAILJET_USER'),
            pass: env('MAILJET_PASS'),
          },
          // ... any custom nodemailer options
        },
        settings: {
          defaultFrom: env('STRAPI_PROD_EMAIL_FROM'),
          defaultReplyTo: env('STRAPI_PROD_EMAIL_REPLYTO'),
        },
      },
    },
     }
 }
