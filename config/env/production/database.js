module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: env('STRAPI_PROD_DB_HOST', '127.0.0.1'),
      port: env.int('STRAPI_PROD_DB_PORT', 1234),
      database: env('STRAPI_PROD_DB_NAME', 'temp'),
      user: env('STRAPI_PROD_DB_USERNAME', 'temp'),
      password: env('STRAPI_PROD_DB_PASSWORD', 'temp'),
      ssl: env.bool('STRAPI_PROD_DB_SSL', true),
    },
  },
});
