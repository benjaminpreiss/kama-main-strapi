module.exports = ({ env }) => ({
  host: env('STRAPI_PROD_HOST', '0.0.0.0'),
  port: env.int('STRAPI_PROD_PORT', 1337),
  app: {
    keys: env.array('STRAPI_PROD_APP_KEYS'),
  },
  url: env('STRAPI_PROD_URL', '')
});
