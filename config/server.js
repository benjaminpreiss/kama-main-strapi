module.exports = ({ env }) => ({
  host: env('STRAPI_DEV_HOST', '0.0.0.0'),
  port: env.int('STRAPI_DEV_PORT', 1337),
  app: {
    keys: env.array('STRAPI_DEV_APP_KEYS'),
  },
  url: env('STRAPI_DEV_URL', '')
});
