module.exports = ({ env }) => ({
  apiToken: {
    salt: env('STRAPI_DEV_API_TOKEN_SALT', 'someRandomLongString'),
  },
  auth: {
    secret: env('STRAPI_DEV_ADMIN_JWT_SECRET', '2349051a118caa62bc0229d08ebb4ab2'),
  },
});
