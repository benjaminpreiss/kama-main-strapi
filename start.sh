#!/bin/bash

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

echo

cd $dir
npm run build
npm run develop