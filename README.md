# Kamalashila main page strapi app

This is the git repo for the kamalashila main pages strapi app, developed in accordance to the 12 factor app standard.

## Getting Started

In order to run this app you will need have to meet the following requirements:

- A running mysql database
- Bash environment variables set for production and development

## bash environment variables

These bash environment variables have to be set in order for this app to work:

**Development:**

```bash
STRAPI_DEV_ADMIN_JWT_SECRET
STRAPI_DEV_DB_HOST
STRAPI_DEV_DB_PORT
STRAPI_DEV_DB_NAME
STRAPI_DEV_DB_USERNAME
STRAPI_DEV_DB_PASSWORD
STRAPI_DEV_DB_SSL
STRAPI_DEV_HOST
STRAPI_DEV_PORT
STRAPI_DEV_APP_KEYS
STRAPI_DEV_URL
```

**Production:**

```bash
STRAPI_PROD_ADMIN_JWT_SECRET
STRAPI_PROD_DB_HOST
STRAPI_PROD_DB_PORT
STRAPI_PROD_DB_NAME
STRAPI_PROD_DB_USERNAME
STRAPI_PROD_DB_PASSWORD
STRAPI_PROD_DB_SSL
STRAPI_PROD_HOST
STRAPI_PROD_PORT
STRAPI_PROD_APP_KEYS
STRAPI_PROD_URL
SERVER_PROD_SSH_USER
SERVER_PROD_IP
SERVER_PROD_PROJECT_PATH
```

## Syncing data with production

To sync data from strapi back and forth between development and production, three steps have to be taken:

- Run clone of git repo during deployment (This will sync the CMS schemas)
- Sync database
- Sync uploads folder via ftp server (This also has to be done from every deployment to the next to preserve data)
