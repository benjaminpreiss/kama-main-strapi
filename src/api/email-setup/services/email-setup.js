'use strict';

/**
 * email-setup service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::email-setup.email-setup');
