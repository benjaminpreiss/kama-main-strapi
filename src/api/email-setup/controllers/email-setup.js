'use strict';

/**
 * email-setup controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::email-setup.email-setup');
