'use strict';

/**
 * email-setup router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::email-setup.email-setup');
