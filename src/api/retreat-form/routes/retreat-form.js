'use strict';

/**
 * retreat-form router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::retreat-form.retreat-form');
