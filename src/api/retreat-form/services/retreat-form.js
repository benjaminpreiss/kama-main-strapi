'use strict';

/**
 * retreat-form service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::retreat-form.retreat-form');
