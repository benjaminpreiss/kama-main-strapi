'use strict';

/**
 *  retreat-form controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::retreat-form.retreat-form');
