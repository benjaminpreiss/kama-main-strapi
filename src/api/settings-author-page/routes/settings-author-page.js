'use strict';

/**
 * settings-author-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::settings-author-page.settings-author-page');
