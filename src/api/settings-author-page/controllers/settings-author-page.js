'use strict';

/**
 *  settings-author-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::settings-author-page.settings-author-page');
