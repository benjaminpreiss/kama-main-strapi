'use strict';

/**
 * settings-author-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::settings-author-page.settings-author-page');
