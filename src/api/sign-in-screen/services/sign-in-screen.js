'use strict';

/**
 * sign-in-screen service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::sign-in-screen.sign-in-screen');
