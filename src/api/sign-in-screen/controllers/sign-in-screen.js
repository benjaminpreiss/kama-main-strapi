'use strict';

/**
 *  sign-in-screen controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::sign-in-screen.sign-in-screen');
