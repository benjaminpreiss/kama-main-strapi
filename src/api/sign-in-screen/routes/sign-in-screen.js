'use strict';

/**
 * sign-in-screen router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::sign-in-screen.sign-in-screen');
