'use strict';

/**
 * newsletter-signup-form router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::newsletter-signup-form.newsletter-signup-form');
