'use strict';

/**
 *  newsletter-signup-form controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::newsletter-signup-form.newsletter-signup-form');
