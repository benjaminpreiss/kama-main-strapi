'use strict';

/**
 * newsletter-signup-form service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::newsletter-signup-form.newsletter-signup-form');
