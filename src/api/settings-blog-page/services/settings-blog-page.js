'use strict';

/**
 * settings-blog-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::settings-blog-page.settings-blog-page');
