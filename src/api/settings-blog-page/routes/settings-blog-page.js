'use strict';

/**
 * settings-blog-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::settings-blog-page.settings-blog-page');
