'use strict';

/**
 *  settings-blog-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::settings-blog-page.settings-blog-page');
