'use strict';

/**
 * settings-course-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::settings-course-page.settings-course-page');
