'use strict';

/**
 *  settings-course-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::settings-course-page.settings-course-page');
