'use strict';

/**
 * settings-course-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::settings-course-page.settings-course-page');
