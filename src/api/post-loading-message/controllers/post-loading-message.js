'use strict';

/**
 *  post-loading-message controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::post-loading-message.post-loading-message');
