'use strict';

/**
 * post-loading-message service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::post-loading-message.post-loading-message');
