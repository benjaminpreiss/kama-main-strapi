'use strict';

/**
 * post-loading-message router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::post-loading-message.post-loading-message');
