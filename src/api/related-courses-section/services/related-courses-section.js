'use strict';

/**
 * related-courses-section service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::related-courses-section.related-courses-section');
