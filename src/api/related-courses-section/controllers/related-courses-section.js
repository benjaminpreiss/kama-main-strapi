'use strict';

/**
 *  related-courses-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::related-courses-section.related-courses-section');
