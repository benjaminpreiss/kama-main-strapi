'use strict';

/**
 * related-courses-section router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::related-courses-section.related-courses-section');
