'use strict';

/**
 * team-members-position service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::team-members-position.team-members-position');
