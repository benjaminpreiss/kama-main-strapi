'use strict';

/**
 *  team-members-position controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::team-members-position.team-members-position');
