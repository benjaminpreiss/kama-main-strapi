'use strict';

/**
 * team-members-position router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::team-members-position.team-members-position');
