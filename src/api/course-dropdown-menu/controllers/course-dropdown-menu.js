'use strict';

/**
 *  course-dropdown-menu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::course-dropdown-menu.course-dropdown-menu');
