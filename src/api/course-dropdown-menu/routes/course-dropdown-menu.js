'use strict';

/**
 * course-dropdown-menu router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::course-dropdown-menu.course-dropdown-menu');
