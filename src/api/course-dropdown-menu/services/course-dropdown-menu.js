'use strict';

/**
 * course-dropdown-menu service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::course-dropdown-menu.course-dropdown-menu');
