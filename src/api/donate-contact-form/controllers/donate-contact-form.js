'use strict';

/**
 *  donate-contact-form controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::donate-contact-form.donate-contact-form');
