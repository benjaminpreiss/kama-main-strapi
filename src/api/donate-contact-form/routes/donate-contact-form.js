'use strict';

/**
 * donate-contact-form router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::donate-contact-form.donate-contact-form');
