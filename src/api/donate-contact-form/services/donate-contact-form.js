'use strict';

/**
 * donate-contact-form service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::donate-contact-form.donate-contact-form');
