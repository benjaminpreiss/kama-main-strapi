'use strict';

/**
 * legal-item router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::legal-item.legal-item');
