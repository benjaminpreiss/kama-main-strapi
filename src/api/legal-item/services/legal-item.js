'use strict';

/**
 * legal-item service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::legal-item.legal-item');
