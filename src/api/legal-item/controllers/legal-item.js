'use strict';

/**
 *  legal-item controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::legal-item.legal-item');
