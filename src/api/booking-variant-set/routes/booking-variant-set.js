'use strict';

/**
 * booking-variant-set router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::booking-variant-set.booking-variant-set');
