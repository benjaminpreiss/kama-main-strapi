'use strict';

/**
 * booking-variant-set service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::booking-variant-set.booking-variant-set');
