'use strict';

/**
 *  booking-variant-set controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::booking-variant-set.booking-variant-set');
