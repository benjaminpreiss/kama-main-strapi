'use strict';

/**
 *  reset-password-screen controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::reset-password-screen.reset-password-screen');
