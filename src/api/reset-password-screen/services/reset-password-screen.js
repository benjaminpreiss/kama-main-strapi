'use strict';

/**
 * reset-password-screen service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::reset-password-screen.reset-password-screen');
