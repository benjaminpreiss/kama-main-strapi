'use strict';

/**
 * reset-password-screen router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::reset-password-screen.reset-password-screen');
