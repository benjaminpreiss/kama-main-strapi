'use strict';

/**
 *  related-blog-post-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::related-blog-post-section.related-blog-post-section');
