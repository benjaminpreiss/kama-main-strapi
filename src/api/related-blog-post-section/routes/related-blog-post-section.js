'use strict';

/**
 * related-blog-post-section router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::related-blog-post-section.related-blog-post-section');
