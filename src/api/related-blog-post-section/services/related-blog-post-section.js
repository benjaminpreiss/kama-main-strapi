'use strict';

/**
 * related-blog-post-section service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::related-blog-post-section.related-blog-post-section');
