'use strict';

/**
 * singe-page-custom service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::singe-page-custom.singe-page-custom');
