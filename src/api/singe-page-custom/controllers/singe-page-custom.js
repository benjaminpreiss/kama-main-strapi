'use strict';

/**
 *  singe-page-custom controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::singe-page-custom.singe-page-custom');
