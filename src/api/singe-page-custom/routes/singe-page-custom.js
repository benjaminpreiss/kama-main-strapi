'use strict';

/**
 * singe-page-custom router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::singe-page-custom.singe-page-custom');
