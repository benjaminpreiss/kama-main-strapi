'use strict';

/**
 * donations-card service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::donations-card.donations-card');
