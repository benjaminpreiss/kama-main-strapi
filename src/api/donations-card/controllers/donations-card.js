'use strict';

/**
 *  donations-card controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::donations-card.donations-card');
