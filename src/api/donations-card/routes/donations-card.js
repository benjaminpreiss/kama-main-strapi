'use strict';

/**
 * donations-card router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::donations-card.donations-card');
