'use strict';

/**
 * confirmation-message service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::confirmation-message.confirmation-message');
