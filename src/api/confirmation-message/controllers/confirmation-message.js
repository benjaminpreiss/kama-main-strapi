'use strict';

/**
 *  confirmation-message controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::confirmation-message.confirmation-message');
