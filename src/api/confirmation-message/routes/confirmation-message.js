'use strict';

/**
 * confirmation-message router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::confirmation-message.confirmation-message');
