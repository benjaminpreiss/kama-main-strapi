'use strict';

/**
 * default-address service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::default-address.default-address');
