'use strict';

/**
 *  default-address controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::default-address.default-address');
