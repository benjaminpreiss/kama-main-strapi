'use strict';

/**
 * default-address router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::default-address.default-address');
