'use strict';

/**
 * forgot-password-screen service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::forgot-password-screen.forgot-password-screen');
