'use strict';

/**
 * forgot-password-screen router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::forgot-password-screen.forgot-password-screen');
