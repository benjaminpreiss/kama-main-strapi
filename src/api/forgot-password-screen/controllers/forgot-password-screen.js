'use strict';

/**
 *  forgot-password-screen controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::forgot-password-screen.forgot-password-screen');
