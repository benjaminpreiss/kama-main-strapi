'use strict';

/**
 * sign-up-screen router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::sign-up-screen.sign-up-screen');
