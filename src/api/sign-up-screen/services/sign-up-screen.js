'use strict';

/**
 * sign-up-screen service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::sign-up-screen.sign-up-screen');
