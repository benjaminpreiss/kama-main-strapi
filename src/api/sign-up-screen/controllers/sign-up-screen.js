'use strict';

/**
 *  sign-up-screen controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::sign-up-screen.sign-up-screen');
