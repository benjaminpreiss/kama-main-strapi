'use strict';

/**
 * sorting-option router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::sorting-option.sorting-option');
