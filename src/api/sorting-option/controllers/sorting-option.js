'use strict';

/**
 *  sorting-option controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::sorting-option.sorting-option');
