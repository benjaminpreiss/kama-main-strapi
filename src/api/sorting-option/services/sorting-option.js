'use strict';

/**
 * sorting-option service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::sorting-option.sorting-option');
