'use strict';

/**
 * mailchimp-setup router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::mailchimp-setup.mailchimp-setup');
