'use strict';

/**
 * mailchimp-setup service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::mailchimp-setup.mailchimp-setup');
