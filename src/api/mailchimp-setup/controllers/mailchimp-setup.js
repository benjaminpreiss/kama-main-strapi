'use strict';

/**
 * mailchimp-setup controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::mailchimp-setup.mailchimp-setup');
